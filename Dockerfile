FROM node:14.16.0 as node
WORKDIR /app
COPY . .
RUN npm cache clean --force
RUN npm install
RUN npm install -g @angular/cli
RUN ng build --prod --base-href /

FROM nginx:alpine
COPY --from=node /app/dist/midass  /usr/share/nginx/html
EXPOSE 80

