import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JioCompleteComponent } from './components/jio-complete/jio-complete.component';
import { JioLoginComponent } from './components/jio-login/jio-login.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'jioLogin',
    component: JioLoginComponent,
  },
  {
    path: 'jio-complete',
    component: JioCompleteComponent,
  },
  {
    path: 'main', loadChildren: () => import(`./components/components.module`).then(m => m.ComponentsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
