import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(public route: Router) {

  }
  canActivate(): any {
    if (localStorage.getItem('device_match')) {

      return true;
    }
    else {
      this.route.navigate(['/login']);
      return false;
    }
  }

}
