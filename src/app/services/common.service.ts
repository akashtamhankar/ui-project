import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class CommonService {

  private Url = environment.Url;
  pollingUrl:any;
  popUpWindow:any;
  constructor(private http: HttpClient) { }

  getToken(data?: any) {
    return this.http.post(`${this.Url}token`, data, { headers: new HttpHeaders().set("Authorization", "Basic NTM1YzljZDUtODI1OS00MWZiLWFlYzItYWQyZGU0OTExOGRlOmQxMjBmY2NjLTQ1YWQtNGZhOS1hNGQ2LTEwZjhiNGM3YzA1YQ==") })
  }

  getSessionUrl(data?: any) {
    return this.http.post(`${this.Url}insights/${data.msidn}`, data, { headers: new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token')!) });
  }

  getPollingData(data?: any) {
    return this.http.get(`${data}`, { headers: new HttpHeaders().set("Authorization", "Basic NTM1YzljZDUtODI1OS00MWZiLWFlYzItYWQyZGU0OTExOGRlOmQxMjBmY2NjLTQ1YWQtNGZhOS1hNGQ2LTEwZjhiNGM3YzA1YQ==") });
  }




}
