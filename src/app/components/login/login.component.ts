import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private CommonService: CommonService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }
  loginForm!: FormGroup;
  ngOnInit(): void {
    this.getAccessToken();
    this.initializaform()
  }
  initializaform() {
    this.loginForm = this.fb.group({
      msidn: ['', Validators.required],

    });
  }


  getAccessToken() {
    let data = {
      "grant_type": "refresh_token",
      "refresh_token": "9415275d-fdf6-4c15-9be2-dbaee600be17",
      "scopes": [
        "device_match"
      ]
    }
    this.spinner.show()
    this.CommonService.getToken(data).subscribe((res: any) => {
      console.log(res.access_token);
      this.spinner.hide()
      localStorage.setItem("token", res.access_token);
    }, err => {
      this.spinner.hide()
      console.log(err);
      Swal.fire({
        title: 'Error',
        text: err,
        icon: 'error',
        confirmButtonText: 'Close',
        confirmButtonColor: 'rgb(52, 106, 255)',
        timer: 5000,
      });
    })
  }

  submit() {
    this.spinner.show();
    let data = {
      msidn: this.loginForm.get('msidn')?.value,
      device_match_notify: {
        notification_uri: "https://service.exmp.com/notify",
        notification_token: "eHabtY1786"
      }
    }
    console.log(data);
    this.CommonService.getSessionUrl(data).subscribe((res: any) => {
      console.log(res);
      let i = new Image();
      i.src = res.device_match.session_uri;
      // this.spinner.hide();

      setTimeout(() => {
        this.getPollingData(res?.device_match?.polling_uri);

      }, 5000);
      this.loginForm.reset();
    }, err => {
      this.spinner.hide();
      console.log(err);
      Swal.fire(
        'Session not created !! Please try again after some time !!',
        '',
        'error'
      );
    })


  }

  getPollingData(req: any) {
    console.log(req);
    let delay = 5000


    let request = setInterval(() => {
      this.CommonService.getPollingData(req).subscribe((res: any) => {
        console.log(res);

        if (res.device_match) {
          this.spinner.hide();
          localStorage.setItem("device_match", res.device_match);
          clearInterval(request);
          Swal.fire({
            title: 'Success',
            text: 'Authentication Successfully!',
            icon: 'success',
            confirmButtonText: 'Close',
            confirmButtonColor: 'rgb(52, 106, 255)',
            timer: 5000,
          });

          this.router.navigateByUrl('/main');

        }

        delay = delay + 5000;
        console.log(delay);


        if (delay == 100000) {
          clearInterval(request)
          this.spinner.hide()
          Swal.fire({
            title: 'Error',
            text: 'Authentication Failed!',
            icon: 'error',
            confirmButtonText: 'Close',
            confirmButtonColor: 'rgb(52, 106, 255)',
            timer: 5000,
          });


        }

      }, err => {
        console.log(err);
        this.spinner.hide()
        Swal.fire({
          title: 'Error',
          text: err,
          icon: 'error',
          confirmButtonText: 'Close',
          confirmButtonColor: 'rgb(52, 106, 255)',
          timer: 5000,
        });


      })
    }, delay);

  }



  isNumber(evt: any) {
    evt = evt ? evt : window.event;
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
