import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JioLoginComponent } from './jio-login.component';

describe('JioLoginComponent', () => {
  let component: JioLoginComponent;
  let fixture: ComponentFixture<JioLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JioLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JioLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
