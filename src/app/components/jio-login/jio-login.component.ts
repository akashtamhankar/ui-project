import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-jio-login',
  templateUrl: './jio-login.component.html',
  styleUrls: ['./jio-login.component.scss']
})
export class JioLoginComponent implements OnInit {

  constructor(private fb :FormBuilder,  private CommonService: CommonService,
    private router: Router,
    private spinner: NgxSpinnerService,private http: HttpClient ) { }

  loginForm!: FormGroup;
  windowRef:any;

  ngOnInit(): void {
    // this.getAccessToken();
    this.initializaform()
  }
  initializaform() {
    this.loginForm = this.fb.group({
      msidn: ['', Validators.required],
    });
  }


  async getAccessToken() {


    return new Promise((resolve, reject) => {

      let data = {
        "grant_type": "refresh_token",
        "refresh_token": "9415275d-fdf6-4c15-9be2-dbaee600be17",
        "scopes": [
          "device_match"
        ]
      }
  
      this.spinner.show()
      this.CommonService.getToken(data).subscribe((res: any) => {
        console.log(res.access_token);
        this.spinner.hide()
        localStorage.setItem("token", res.access_token);
        resolve(" ")
      }, err => {
        reject(" ");
        this.spinner.hide()
        console.log(err);
        Swal.fire({
          title: 'Error',
          text: err,
          icon: 'error',
          confirmButtonText: 'Close',
          confirmButtonColor: 'rgb(52, 106, 255)',
          timer: 5000,
        });
      })

    })
  
    
  }


  submit() {
    if((window as any)?.name)(window as any)?.close();
    this.getAccessToken().then(res=>{
      setTimeout(()=>{
        this.spinner.show();
        let data = {
          msidn: this.loginForm.get('msidn')?.value,
          device_match_notify: {
            redirect_uri: "https://mid.rmlconnect.net/#/jio-complete"
          }
        }
        this.CommonService.getSessionUrl(data).subscribe((res: any) => {
          console.log(res);
         this.spinner.hide();
        let myWindow:any = window.open(res?.device_match?.session_uri, "PopUpWindow", "left=450,top=350,width=650,height=550");
    
         myWindow["pollingUrl"]=res.device_match.polling_uri; // Adding required data to window obj so that it can be used on pop-up page
         myWindow["phoneNumber"]=this.loginForm.get('msidn')?.value;
    
         setTimeout(()=>this.loginForm.reset(),100);
    
        }, err => {
          this.spinner.hide();
          console.log(err);
          Swal.fire(
            'Session not created !! Please try again after some time !!',
            '',
            'error'
          );
        })
      },500)
    })
   
  }


  isNumber(evt: any) {
    evt = evt ? evt : window.event;
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}
