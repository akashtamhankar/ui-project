import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-jio-complete',
  templateUrl: './jio-complete.component.html',
  styleUrls: ['./jio-complete.component.scss'],
})
export class JioCompleteComponent implements OnInit {
  authStatus: boolean = false;
  showAuthStatus: boolean = false;
  constructor(
    private commonService: CommonService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {}

  ngOnInit(): void {
    if ((window as any)?.pollingUrl) {
      this.performPollingOperation((window as any)?.pollingUrl);
    }else{
      Swal.fire({
        title: 'Error',
        text: 'Something Went wrong, Please try again !!!',
        icon: 'error',
        confirmButtonText: 'Close',
        confirmButtonColor: 'rgb(52, 106, 255)',
        timer: 2500,
      });
      setTimeout(()=>(window as any).close(),2000) ;

    }
  }

  performPollingOperation(url:any) {
    console.log(url)
    let delay = 3000;
    let request = setInterval(() => {
      this.commonService.getPollingData(url).subscribe(
        (res: any) => {

          if (res.device_match) {
            this.authStatus = true;
            this.showAuthStatus = true;

            this.spinner.hide();
            localStorage.setItem('device_match', res.device_match);
            clearInterval(request);
            Swal.fire({
              title: 'Success',
              text: 'Authentication Successfully!',
              icon: 'success',
              confirmButtonText: 'Close',
              confirmButtonColor: 'rgb(52, 106, 255)',
              timer: 5000,
            });

           setTimeout(()=>(window as any).close(),2000) ;
          }

          delay = delay + 3000;
          console.log(delay);

          if (delay > 6000) {
            clearInterval(request);
            this.authStatus = false;
            this.showAuthStatus = true;
            this.spinner.hide();
            Swal.fire({
              title: 'Error',
              text: 'Authentication Failed!',
              icon: 'error',
              confirmButtonText: 'Close',
              confirmButtonColor: 'rgb(52, 106, 255)',
              timer: 5000,
            });

            this.getAccessTokenForOTP(); // NOW  GO-TO SMS-OTP FLOW
          }
        },
        (err) => {
          console.log(err);
          this.spinner.hide();
          Swal.fire({
            title: 'Error',
            text: err,
            icon: 'error',
            confirmButtonText: 'Close',
            confirmButtonColor: 'rgb(52, 106, 255)',
            timer: 5000,
          });
        }
      );
    }, delay);
  }

  getAccessTokenForOTP() {
    let data = {
      grant_type: 'refresh_token',
      refresh_token: '9415275d-fdf6-4c15-9be2-dbaee600be18',
      scopes: ['safr_auth'],
    };

    this.spinner.show();
    this.commonService.getToken(data).subscribe(
      (res: any) => {
        console.log(res.access_token);
        localStorage.setItem('token', res.access_token);
        this.commonService
          .getSessionUrl({ msidn: (window as any)?.phoneNumber })
          .subscribe(
            (res: any) => {
              console.log('SAFR RES ', res);
              this.spinner.hide();

              // (window as any).close();
              let newWindow = window.open(
                res?.safr_auth?.session_uri,
                'otpPopUp',
                'left=450,top=350,width=650,height=550'
              );

              let checkInterval = setInterval(() => {
                if (newWindow?.closed) {
                  this.performPollingOperation(res.safr_auth.polling_uri)
                  clearInterval(checkInterval)
                }
              }, 500);
            },
            (err) => {
              this.spinner.hide();
              console.log(err);
              Swal.fire(
                'Session not created !! Please try again after some time !!',
                '',
                'error'
              );
            }
          );
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
        Swal.fire({
          title: 'Error',
          text: err,
          icon: 'error',
          confirmButtonText: 'Close',
          confirmButtonColor: 'rgb(52, 106, 255)',
          timer: 5000,
        });
      }
    );
  }
}
