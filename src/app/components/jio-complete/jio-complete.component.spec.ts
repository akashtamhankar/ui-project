import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JioCompleteComponent } from './jio-complete.component';

describe('JioCompleteComponent', () => {
  let component: JioCompleteComponent;
  let fixture: ComponentFixture<JioCompleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JioCompleteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JioCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
