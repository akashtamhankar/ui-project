import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ComponentsComponent } from './components.component';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { AuthGuard } from '../services/auth.guard';
import { JioLoginComponent } from './jio-login/jio-login.component';
import { JioCompleteComponent } from './jio-complete/jio-complete.component';

const routes: Routes = [

  {
    path: '', component: ComponentsComponent, canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'about', component: AboutComponent },


    ]
  },
]


@NgModule({
  declarations: [ComponentsComponent, HomeComponent, AboutComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule, HttpClientModule,
    NgxSpinnerModule,
    RouterModule.forChild(routes)

  ]
})
export class ComponentsModule { }
